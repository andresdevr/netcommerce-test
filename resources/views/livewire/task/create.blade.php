
<div class="mt-5 md:mt-0 md:col-span-2">
    <form wire:submit.prevent="submit">
        <div class="shadow sm:rounded-md sm:overflow-hidden">
          <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
            <div>
                <label class="block text-sm font-medium text-gray-700">
                    {{ __('Task') }}
                </label>
                <div class="mt-1">
                    <textarea rows="2" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('Do something') }}..." wire:model="name"></textarea>
                    @error('name')
                        <span class="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                            {{ $message }}
                        </span>
                    @enderror
                </div>
            </div>
            @if(Auth::user()->hasRole('administrator'))
                <div>
                    <label class="block text-sm font-medium text-gray-700">
                        {{ __('Assigned To') }}
                    </label>
                    <livewire:user.user-select>
                    </lviewire:user.user-select>   
                </div>
            @endif
            <div class="grid grid-cols-2 gap-2">
                <div>
                    <label class="block text-sm font-medium text-gray-700">
                        {{ _('Start date') }}
                    </label>
                    <input class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" type="date" wire:model="startDate" />
                    @error('startDate')
                        <span class="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                            {{ $message }}        
                        </span>
                    @enderror
                    
                </div>
                <div>
                    <label class="block text-sm font-medium text-gray-700">
                        {{ _('Expiration date') }}
                    </label>
                    <input class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" type="date" wire:model="expirationDate" />
                    @error('expirationDate')
                        <span class="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                            {{ $message }}
                        </span>
                    @enderror
                </div>
            </div>
            <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Create
                </button>
            </div>
      </form>
    </div>