<tr class="{{ $task->is_completed ? 'opacity-25' : ''}}">
    <td class="px-6 py-2 max-w-xs">
        @if(! $edit)
            {{ $task->name }}
        @else
            <div>
                <label class="block text-sm font-medium text-gray-700">
                    {{ __('Task') }}
                </label>
                <div class="mt-1">
                    <textarea rows="2" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('Do something') }}..." wire:model="task.name"></textarea>
                </div>
            </div>
        @endif
    </td>
    <td class="px-6 py-4 whitespace-nowrap hidden hidden xl:table-cell lg:table-cell md:table-cell">
        <div class="flex items-center">
            <div class="flex-shrink-0 h-10 w-10">
            <img class="h-10 w-10 rounded-full" src="{{ $task->assigned->profilePhotoUrl }}" alt="">
            </div>
            <div class="ml-4">
                <div class="text-sm font-medium text-gray-900">
                        {{ $task->assigned->name }}
                </div>
            </div>
        </div>
    </td>
    <td class="px-6 py-4 whitespace-nowrap hidden xl:table-cell lg:table-cell">
        <div class="flex items-center">
            <div class="flex-shrink-0 h-10 w-10">
              <img class="h-10 w-10 rounded-full" src="{{ $task->creator->profilePhotoUrl }}" alt="">
            </div>
            <div class="ml-4">
              <div class="text-sm font-medium text-gray-900">
                    {{ $task->creator->name }}
              </div>
            </div>
        </div> 
    </td>
    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500 hidden xl:table-cell lg:table-cell md:table-cell">
        @if (! $edit)
            <div class="flex items-center gap-2">
                <div class="h-2 w-2 rounded-full {{ $task->present()->isStarted() ? 'bg-green-300' : 'bg-gray-300' }} ">
                </div>
                {{ $task->present()->startDateCarbon()->format('d-M-Y') }}
            </div>
            @if($task->present()->daysLeftToStart() > 0)
                <span class="text-center">
                    {{ $task->present()->daysLeftToStart()  . __(' Days left') }}
                </span>
            @elseif($task->present()->daysLeftToExpire() == 0)
                <span class="text-center">
                    {{  __('Today') }}
                </span>
            @endif
        @else
            <div>
                <label class="block text-sm font-medium text-gray-700">
                    {{ __('Start date') }}
                </label>
                <input class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" type="date" wire:model="task.start_date" />
            </div>  
        @endif
    </td>
    <td class="px-6 py-4 whitespace-nowrap text-sm">
        @if(! $edit)
            <div class="flex items-center gap-2">
                <div class="h-2 w-2 rounded-full {{ $task->present()->isStarted() ? '' : 'bg-gray-300' }} {{ !$task->present()->isExpirated() && $task->present()->isStarted() ? 'bg-green-300' : '' }} {{ $task->present()->isExpirated() ? 'bg-red-300' : '' }} ">
                </div>
                {{ $task->present()->expirationDateCarbon()->format('d-M-Y') }}
            </div>
            @if($task->present()->daysLeftToExpire() > 0)
                <span class="text-center">
                    {{ $task->present()->daysLeftToExpire()  . __(' Days left') }}
                </span>
            @elseif($task->present()->daysLeftToExpire() == 0)
                <span class="text-center">
                    {{  __('Today') }}
                </span>
            @endif
        @else
            <div>
                <label class="block text-sm font-medium text-gray-700">
                    {{ __('Expiration date') }}
                </label>
                <input class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" type="date" wire:model="task.expiration_date" />
            </div>
        @endif
    </td>
    <td class="px-6 py-4 whitespace-nowrap text-sm grid grid-cols-1 xl:grid-cols-2 lg:grid-cols-2 gap-1 xl:gap-0 lg:gap-0">
        @if(!$edit)
            @if (Auth::user()->hasRole('administrator') || Auth::user()->id == $task->creator_id)
                <button class="focus:outline-none text-sm w-full xl:w-10 lg:w-10 md:w-10 py-3 rounded-md font-semibold text-blue-600 bg-blue-100 ring-1 mx-auto xl:m-0 lg:m-0 md:m-0" wire:click="$toggle('edit')">
                    {{ __('Edit') }}
                </button>
            @endif
            @if ($task->is_completed)
                <button class="focus:outline-none text-sm w-full xl:w-24 lg:w-24 md:w-24 py-3 rounded-md font-semibold text-blue-600 bg-blue-100 ring-1 mx-auto xl:m-0 lg:m-0 md:m-0" wire:click="toggleComplete">
                    {{ __('Uncomplete') }}
                </button>
            @else
                <button class="focus:outline-none text-sm w-full xl:w-24 lg:w-24 md:w-24 py-3 rounded-md font-semibold text-white bg-blue-500 ring-2 mx-auto xl:m-0 lg:m-0 md:m-0" wire:click="toggleComplete">
                    {{ __('Complete') }}
                </button>
            @endif
        @else
            <button class="focus:outline-none text-sm w-full xl:w-14 lg:w-14 md:w-14 py-3 rounded-md font-semibold text-blue-600 bg-blue-100 ring-1 mx-auto xl:m-0 lg:m-0 md:m-0" wire:click="$toggle('edit')">
                {{ __('Cancel') }}
            </button>
            <button class="focus:outline-none text-sm w-full xl:w-20 lg:w-20 md:w-20 py-3 rounded-md font-semibold text-white bg-green-500 ring-1 mx-auto xl:m-0 lg:m-0 md:m-0" wire:click="edit">
                {{ __('Save') }}
            </button>
        @endif
    </td>
</tr>
