<div>
    <table class="min-w-full divide-y divide-gray-200">
        <div class="grid xl:grid-cols-5 grid-cols-1">
            <div class="p-3 flex rounded-md shadow-sm  {{ Auth::user()->hasRole('administrator') ? 'col-span-3' : 'col-span-4' }}">
                <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                    {{ __('Search') }}
                </span>
                <input type="text" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300" placeholder="{{ __('Task') }}..." wire:model="search">
            </div>
            @if (Auth::user()->hasRole('administrator'))
                <div class="p-3 flex rounded-md shadow-sm">
                    <livewire:user.user-select>
                    </livewire:user.user-select>
                </div>
            @endif
            <div class="p-3 rounded-md shadow-sm">
                <select class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-gray-300 col-span-1" wire:model="status">
                    <option value="">
                        {{ __('Uncompleted') }}
                    </option>
                    <option value="completed">
                        {{ __('Completed') }}
                    </option>
                    <option value="all">
                        {{ __('All') }}
                    </option>
                </select>
            </div>
          </div>
        <thead class="bg-gray-50">
            <tr>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                {{ __('Task') }} 
                </th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider hidden xl:table-cell lg:table-cell md:table-cell">
                    {{ __('Assigned to') }}
                </th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider hidden xl:table-cell lg:table-cell">
                    {{ __('Assigned by') }}
                </th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider hidden xl:table-cell lg:table-cell md:table-cell">
                    {{ __('Start Date') }}
                </th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    {{ __('Expiration date') }}
                </th>
                <th scope="col" class="relative px-6 py-3">

                </th>
            </tr>
        </thead>
        <tbody class="bg-white divide-y divide-gray-200 max-h-16">
            @forelse($tasks as $task)
                <livewire:task.task :task="$task"  :key="'task-list-' . $task->id">
                </livewire:task.task>
            @empty
            <tr>
                <th colspan="6" class="py-5">
                    <h3>
                        {{ __('Without tasks yet') }}.
                    </h3>
                </th>
            </tr>
            @endforelse
            
        </tbody>
    </table>
    <div class="bg-white px-4 py-3  border-t border-gray-200 sm:px-6">
        {{ $tasks->links() }}
    </div>
    <livewire:task.create>
    </livewire:task.create>
</div>
