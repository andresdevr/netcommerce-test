<select class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-gray-300 col-span-1" wire:model="userId" wire:change="selectUser($event.target.value)">
    @foreach ($users as $user)
        <option value="{{ $user->id}}">
            {{ $user->name }}
        </option>
    @endforeach

</select>