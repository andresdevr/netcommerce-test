<div>
    <h2>
        Roles
    </h2>
    <div>
        @foreach($roles as $role)
            <a class="inline-block rounded-full text-white {{  $user->hasRole($role->name) ? 'bg-blue-400 hover:bg-blue-500' : 'bg-gray-400 hover:bg-gray-500' }} duration-300 text-xs font-bold mr-1 md:mr-2 mb-2 px-2 md:px-4 py-1 opacity-90 hover:opacity-100 cursor-pointer" wire:click="toggleRole('{{$role->name}}')">
                {{ $role->name }}
                @if($user->hasRole($role->name))
                    <span>
                        -
                    </span>
                @else
                    <span>
                        +
                    </span>
                @endif
            </a>
        @endforeach
    </div>
</div>
