<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Users') }} > {{ $user->name }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <form action="{{ route('users.update', ['user' => $user->id]) }}" method="POST" id="edit" name="edit">
                    @csrf()
                    @method('PUT')
                    <div class="shadow sm:rounded-md sm:overflow-hidden">
                        <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                            <div class="grid grid-cols-4 gap-6">
                                <div class="col-span-4 sm:col-span-2">
                                    <label for="company_website" class="block text-sm font-medium text-gray-700">
                                        {{ __('Name') }}
                                    </label>
                                    <div class="mt-1 flex rounded-md shadow-sm">
                                        <input type="text" name="name" id="name" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-gray-300" placeholder="{{ __('Name') }}" value="{{ $user->name }}">
                                        @error('name')
                                        <span class="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                                            {{ $message }}        
                                        </span>
                                    @enderror
                                    </div>
                                </div>
                                <div class="col-span-4 sm:col-span-2">
                                    <label for="company_website" class="block text-sm font-medium text-gray-700">
                                        {{ __('Email') }}
                                    </label>
                                    <div class="mt-1 rounded-md shadow-sm">
                                        <input type="email" name="email" id="email" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-gray-300" placeholder="{{ __('Email') }}" value="{{ $user->email }}">
                                        @error('email')
                                            <span class="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                                                {{ $message }}        
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-span-4 sm:col-span-2">
                                    <label for="company_website" class="block text-sm font-medium text-gray-700">
                                        {{ __('Password') }}
                                    </label>
                                    <div class="mt-1 rounded-md shadow-sm">
                                        <input name="password" id="password" type="password" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-gray-300" placeholder="{{ __('New password') }}">
                                        @error('password')
                                            <span class="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                                                {{ $message }}        
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-span-4 sm:col-span-2">
                                    <label for="company_website" class="block text-sm font-medium text-gray-700">
                                        {{ __('Confirm password') }}
                                    </label>
                                    <input name="password_confirmation" id="password_confirmation" type="password" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-gray-300" placeholder="{{ __('Confirm new password') }}">
                                    <div class="mt-1 flex rounded-md shadow-sm">
                                    </div>
                                </div>
                            </div>
                            <div>
                                <livewire:user.role-toggle :user="$user">
                                </livewire:user.role-toggle>
                            </div>
               
                        </div>
                    </div>
                </form>
                <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                    @if($user->id != 1)
                        <form action="{{ route('users.destroy', ['user' => $user->id]) }}" method="POST" id="delete" name="delete">
                            @csrf()
                            @method('DELETE')
                            <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500" form="delete">
                                {{ __('Delete') }}
                            </button>
                        </form>
                    @endif
                    <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" form="edit">
                        {{ __('Save') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>