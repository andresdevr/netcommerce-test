<?php

namespace App\Presenters;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TaskPresenter extends ModelPresenter
{
    private $date;
    private $start_date_carbon;
    private $expiration_date_carbon;

    public function __construct(Model $model)
    {
        parent::__construct($model);
        $this->date = new Carbon;
        $this->start_date_carbon = new Carbon($model->start_date);
        $this->expiration_date_carbon = new Carbon($model->expiration_date);
    }

    public function isExpirated()
    {
        return $this->expiration_date_carbon->lessThan($this->date);
    }

    public function isStarted()
    {
        return $this->start_date_carbon->lessThan($this->date);
    }

    public function daysLeftToStart()
    {
        return $this->date->diffInDays($this->start_date_carbon, false);
    }

    public function daysLeftToExpire()
    {
        return $this->date->diffInDays($this->expiration_date_carbon, false);
    }

    public function startDateCarbon()
    {
        return new Carbon($this->start_date_carbon);
    }

    public function expirationDateCarbon()
    {
        return new Carbon($this->expiration_date_carbon);
    }
}