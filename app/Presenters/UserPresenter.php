<?php

namespace App\Presenters;

class UserPresenter extends ModelPresenter
{
    public function roles()
    {
        return $this->model->roles->pluck('name')->join(', ');
    }
}