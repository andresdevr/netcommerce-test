<?php

namespace App\Models;

use App\Presenters\TaskPresenter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'start_date',
        'expiration_date',
        'assigned_id',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'start_date' => 'datetime:Y-m-d',
        'expiration_date' => 'datetime:Y-m-d',
    ];
   

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public function assigned()
    {
        return $this->belongsTo(User::class, 'assigned_id');
    }

    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = new Carbon($value);
    }

    public function setExpirationDateAttribute($value)
    {
        $this->attributes['expiration_date'] = new Carbon($value);
    }


    public function scopeWhereLike($query, string $column, string $search)
    {
        return $query->where($column, 'LIKE', "%$search%");
    }

    public function scopeCompleteStatus($query, string $status)
    {
        if ($status == 'completed')
        {
            return $query->where('is_completed', true);
        }
        else if ($status == 'all')
        {
            return $query;
        }

        return $query->where('is_completed', false);
    }

    public function scopeAssignedUser($query, $userId)
    {
        if (is_null($userId))
        {
            return $query;
        }
        
        return $query->where('assigned_id', $userId);
    }

    public function present()
    {
        return new TaskPresenter($this);
    }
}
