<?php

namespace App\Http\Livewire\Task;

use Livewire\Component;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Task extends Component
{
    public $task;

    public $edit = false;

    use AuthorizesRequests;

    protected $rules = [
        'task.name' => 'required|max:255',
        'task.start_date' => 'required|date',
        'task.expiration_date' => 'required|date|after:start_date'
    ];


    public function render()
    {
        return view('livewire.task.task');
    }

    public function toggleComplete()
    {
        $this->authorize('task-complete', $this->task);
        $this->task->is_completed = !$this->task->is_completed;

        $this->task->save();
    }

    public function edit()
    {
        $this->authorize('task-update', $this->task);

        $this->task->save();

        $this->edit = false;
    }
}
