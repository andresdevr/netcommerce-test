<?php

namespace App\Http\Livewire\Task;

use Livewire\Component;
use App\Models\Task;
use Illuminate\Support\Facades\Auth;

class Create extends Component
{

    public $name;

    public $startDate;

    public $expirationDate;

    public $assignedUser;

    protected $rules = [
        'name' => 'required|max:255',
        'startDate' => 'required|date',
        'expirationDate' => 'required|date|after_or_equal:startDate',
        'assignedUser' => ''
    ];

    protected $listeners = [
        'userSelected' => 'assignUser'
    ];

    public function render()
    {
        return view('livewire.task.create');
    }

    public function submit()
    {
        $this->validate();

        $user = Auth::user();

        $user->createdTasks()->create([
            'name' => $this->name,
            'start_date' => $this->startDate,
            'expiration_date' => $this->expirationDate,
            'assigned_id' => ($user->hasRole('administrator') && $this->assignedUser) ? $this->assignedUser : $user->id 
        ]);


        $this->emitUp('taskCreated');


        
    }

    public function assignUser(int $userId)
    {
        $this->assignedUser = $userId;
    }


}
