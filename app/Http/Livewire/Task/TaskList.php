<?php

namespace App\Http\Livewire\Task;

use App\Models\Task;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class TaskList extends Component
{
    use WithPagination;

    public $search = '';

    public $status = '';

    public $assignedId;

    protected $listeners = [
        'taskCreated' => 'updateTasks',
        'userSelected' => 'filterByUser'
    ];

    public function render()
    {
        return view('livewire.task.task-list', [
            'tasks' => $this->getTasks()
        ]);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    private function getTasks()
    {
        $user = Auth::user();

        if ($user->hasRole('administrator'))
        {
            $query = Task::query(); 
            $query->assignedUser($this->assignedId);    
        }
        else
        {
            $query = $user->tasks();
        }

        return $query->with('creator', 'assigned')
                    ->whereLike('name', $this->search)
                    ->completeStatus($this->status)
                    ->orderBy('expiration_date', 'asc')
                    ->paginate();


    }

    public function updateTasks()
    {
        return redirect()->route('dashboard');
    }

    public function filterByUser($value)
    {
        $this->assignedId = $value;
    }
}
