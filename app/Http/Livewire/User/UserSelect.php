<?php

namespace App\Http\Livewire\User;

use Livewire\Component;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserSelect extends Component
{

    public $userId;

    public function mount()
    {
        $this->fill(['userId' => Auth::id()]);
    }

    public function render()
    {
        return view('livewire.user.user-select', [
            'users' => $this->getUsers()
        ]);
    }

    private function getUsers()
    {
        return User::select('id', 'name')->get();
    }

    public function selectUser(int $userId)
    {
        $this->emitUp('userSelected', $userId);
    }
}
