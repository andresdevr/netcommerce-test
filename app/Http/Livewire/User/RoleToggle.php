<?php

namespace App\Http\Livewire\User;

use Livewire\Component;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class RoleToggle extends Component
{
    public $user;

    public function mount(User $user)
    {
        $this->fill(['user' => $user]);
    }

    public function render()
    {
        return view('livewire.user.role-toggle', [
            'roles' => $this->getRoles()
        ]);
    }

    private function getRoles()
    {
        return Role::get();
    }

    public function toggleRole(string $role)
    {
        if (!Auth::user()->hasRole('administrator') || $this->user->id == 1)
        {
            return ;
        }
        if ($this->user->hasRole($role))
        {
            $this->user->removeRole($role);
        }
        else
        {
            $this->user->assignRole($role);
        }
    }
}
