<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Admin User Test',
            'email' => 'admin@test.com',
            'password' => Hash::make('password'),
        ]);

        $user->assignRole('administrator');

        $user = User::create([
            'name' => 'Regular User Test',
            'email' => 'regular@test.com',
            'password' => Hash::make('password'),
        ]);

        $user->assignRole('regular');
    }
}
