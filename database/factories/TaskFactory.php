<?php

namespace Database\Factories;

use App\Models\Task;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->paragraph,
            'assigned_id' => User::factory(),
            'creator_id' => User::factory(),
            'start_date' => $this->faker->dateTimeThisMonth('now'),
            'expiration_date' => $this->faker->dateTimeBetween('now', '+1 year')
        ];
    }
}
