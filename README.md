# Netcommerce Test

Following the specifications from the `Examen_tipo_1.pdf` file

## Specifications

#### Examen práctico:  
Creación de un ABM de Tareas con Laravel 8* 
#### Descripción: 
Crear un administrador de tareas. 
###### Módulo tareas: 
Crear un listado y ABM de las tareas.  
El administrador tendrá todos los permisos de este módulo.  
El usuario solo puede completar las tareas que se le asigno.  
Un usuario puede crear sus propias tareas. Incluir búsqueda de tareas por usuario nombre de la tarea.  
Datos de tareas: 
- Nombre de tarea. 
- Usuario asignado a la tarea. 
- Usuario que creó la tarea. 
- Fecha de inicio  
- Fecha de vencimiento  
###### Modulo Usuarios. 
Crear un listado y ABM de los usuarios,  
Se asignarán 2 Roles Administrador y Usuario.  
Solo el Administrador tendrá acceso a esta sección.  
Requerimientos  
- Integrar un paquete para el login de usuarios  
- Usar “Collections” y “Eloquent” para consulta de datos.  
- Una vista simple en Laravel para el ABM.  
- Subir el proyecto a algún repositorio GIT de tu preferencia, esto para su revisión.  
- Publicar el sitio (proporcionar la url y datos). 


*Fecha límite de entrega martes 2 de marzo del 2021.*

## Project Details

### Auth packages used
- [Laravel Jetstream](https://jetstream.laravel.com) powered by [Laravel Fortify](https://laravel.com/docs/8.x/fortify) And[Laravel-permission](https://spatie.be/docs/laravel-permission/v3/introduction) to manage roles and permissions.

### Url test
- domain: [http://netcommerce.andreycarlo.com](http://netcommerce.andreycarlo.com])  
- IP: [128.199.0.226](128.199.0.226)

### Auth test

#### Administrator
**email:** admin@test.com  
**password:** password  
  
#### User
**email:** regular@test.com  
**password:** password
